//
//  HomeViewController.swift
//  Kids_Learn_App
//
//  Created by NAGESH RANGAPURE on 21/08/17.
//  Copyright © 2017 Nagesh Rangapure. All rights reserved.
//

import UIKit
import AVFoundation

class HomeViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate{
    @IBOutlet weak var homeView: UICollectionView!
    
    @IBOutlet weak var btnSun: UIButton!
    
    @IBOutlet weak var btnMoney: UIButton!
    private var audioPlayer: AVAudioPlayer?

    var imageArray = [UIImage(named: "fruits"),UIImage(named: "vegetable"),UIImage(named: "animals"),UIImage(named: "flower"),UIImage(named: "vehicles")]
    
var name = ["FRUITS","VEGETABLES","ANIMALS","FLOWERS","VEHICLES"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSun.pulsate()
        btnMoney.shake()
        
        self.homeView.isPagingEnabled = true
        
        //self.navigationController?.isNavigationBarHidden = false
        //collectionView.dataSource = self;

    }
    
    override func viewWillAppear(_ animated: Bool) {
                super.viewWillAppear(animated)
        
                self.navigationController?.isNavigationBarHidden = false
            }
    

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellImage", for: indexPath) as! HomeCollectionViewCell
        
        cell.lblName.text = name[indexPath.row]
        cell.cellImage.image = imageArray[indexPath.row]
        
    return  cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if ((indexPath.row) == 0) {
            
            let fruit : FruitsViewController = self.storyboard!.instantiateViewController(withIdentifier: "fruits")as! FruitsViewController
            self.navigationController?.pushViewController(fruit, animated: true)
        }else if ((indexPath.row) == 1) {
            
            let vegetable :VegetablesViewController = self.storyboard!.instantiateViewController(withIdentifier: "vegetables") as! VegetablesViewController
            
            self.navigationController?.pushViewController(vegetable, animated: true)
        } else if((indexPath.row) == 2){
            
            let animal :AnimalViewController = self.storyboard!.instantiateViewController(withIdentifier: "animals") as!AnimalViewController
            self.navigationController?.pushViewController(animal, animated: true)
            
        }else if((indexPath.row) == 3){
            
            let flower : FlowerViewController = self.storyboard!.instantiateViewController(withIdentifier: "flower")as! FlowerViewController
            self.navigationController?.pushViewController(flower, animated: true)
        }else if((indexPath.row) == 4){
            
            displayMyAlert(message: "")

        }
    }
    
    func displayMyAlert(message: String){
        
        let myAlert = UIAlertController(title:"Comming Soon...",message:message,preferredStyle:UIAlertControllerStyle.alert)
        
        self.present(myAlert,animated: true,completion: nil)
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnSunPress(_ sender: UIButton) {
//        sender.pulsate()
//        let alertSound = URL(fileURLWithPath: Bundle.main.path(forResource: "Fire2", ofType: "wav")!)
//        print(alertSound)
//
//        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
//        try! AVAudioSession.sharedInstance().setActive(true)
//
//        try! audioPlayer = AVAudioPlayer(contentsOf: alertSound)
//        audioPlayer!.prepareToPlay()
//        audioPlayer!.play()
    }
    @IBAction func btnPressMonkey(_ sender: UIButton) {
//        sender.shake()
//        let alertSound = URL(fileURLWithPath: Bundle.main.path(forResource: "Mario", ofType: "wav")!)
//        print(alertSound)
//        
//        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
//        try! AVAudioSession.sharedInstance().setActive(true)
//        
//        try! audioPlayer = AVAudioPlayer(contentsOf: alertSound)
//        audioPlayer!.prepareToPlay()
//        audioPlayer!.play()
        

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIButton {
    
    func flash() {
        
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 0.5
        flash.fromValue = 1
        flash.toValue = 0.1
        flash.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = 3
        
        layer.add(flash, forKey: nil)
    }
    
    
    func shake() {
        
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.1
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        
        layer.add(shake, forKey: "position")
    }
    func pulsate() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.6
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = true
        pulse.repeatCount = 2
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        
        layer.add(pulse, forKey: "pulse")
    }
}

