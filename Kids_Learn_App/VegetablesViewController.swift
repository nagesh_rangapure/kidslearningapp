//
//  VegetablesViewController.swift
//  Kids_Learn_App
//
//  Created by NAGESH RANGAPURE on 16/08/17.
//  Copyright © 2017 Nagesh Rangapure. All rights reserved.
//

import UIKit

class VegetablesViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var vegetableView: UICollectionView!
    let vegetableslist = [UIImage(named:"tomato"),UIImage(named:"potato"),UIImage(named:"carrot") ,UIImage(named:"peas"),UIImage(named:"cabbage"),UIImage(named:"redchili"),UIImage(named:"cucumber"),UIImage(named:"Brinjal")]
    
    
    let vegetableName = ["TOMATO","POTATO","CARROT","PEA","CABBAGE","REDCHILI","CUCUMBER","BRINJAL"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.vegetableView.isPagingEnabled = true
        
        self.navigationController?.isNavigationBarHidden = true

        
        // Do any additional setup after loading the view.
    }
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return vegetableslist.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "vegetableCell", for: indexPath)as! VegetablesCollectionViewCell
        
        cell.imgVegetable.image = vegetableslist[indexPath.row]
        cell.lblName.text = vegetableName[indexPath.row]
        
        cell.btnNext.isHidden = (indexPath.row == collectionView.numberOfItems(inSection: 0) - 1)
        cell.btnPrevious.isHidden = (indexPath.row == 0)
        
        return cell
    }

    @IBAction func btnClickBack(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func btnClickedNext(_ sender: Any) {
        
        let collectionBounds = self.vegetableView.bounds
        let contentOffset = CGFloat(floor(self.vegetableView.contentOffset.x + collectionBounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    
    @IBAction func btnClickedPrevious(_ sender: Any) {
        let collectionBounds = self.vegetableView.bounds
        let contentOffset = CGFloat(floor(self.vegetableView.contentOffset.x - collectionBounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    
    func moveToFrame(contentOffset : CGFloat) {
        
        let frame: CGRect = CGRect(x : contentOffset ,y : self.vegetableView.contentOffset.y ,width : self.vegetableView.frame.width,height : self.vegetableView.frame.height)
        self.vegetableView.scrollRectToVisible(frame, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
