//
//  FruitQuizViewController.swift
//  Kids_Learn_App
//
//  Created by NAGESH RANGAPURE on 08/09/17.
//  Copyright © 2017 Nagesh Rangapure. All rights reserved.
//

import UIKit
import AVFoundation
class FruitQuizViewController: UIViewController {

    @IBOutlet weak var lblQuestion: UILabel!
    
    @IBOutlet weak var lblAnswer: UILabel!
    
    @IBOutlet weak var btnQuestion1: UIButton!
    
    @IBOutlet weak var btnQuestion2: UIButton!
    
    @IBOutlet weak var btnQuestion3: UIButton!
    
    @IBOutlet weak var btnQuestion4: UIButton!
    
    var CorrectAnswer = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        //Hide()
        RamdomQuestions()
        
        // Do any additional setup after loading the view.
    }
    
    func RamdomQuestions () {
        var RandomNumber = arc4random() % 5
        RandomNumber += 1
        
        switch (RandomNumber) {
        case 1:
            lblQuestion.text = "Where is the Apple?"
            
            let utterance = AVSpeechUtterance(string:lblQuestion.text!)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)
            
            let image1  = UIImage(named: "apple") as UIImage?
            let image2  = UIImage(named: "banana") as UIImage?
            let image3 = UIImage(named: "cherries") as UIImage?
            let image4  = UIImage(named: "Grapes") as UIImage?
            
            btnQuestion1.setBackgroundImage(image2, for: UIControlState.normal)
            btnQuestion2.setBackgroundImage(image1, for: UIControlState.normal)
            btnQuestion3.setBackgroundImage(image3, for: UIControlState.normal)
            btnQuestion4.setBackgroundImage(image4, for: UIControlState.normal)
            CorrectAnswer = "2"
            
            break
        case 2:
            lblQuestion.text = "Where is the Pineapple?"
            
            //let string = "Where is the Peach?!"
            let utterance = AVSpeechUtterance(string:lblQuestion.text!)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)

            
            let image1  = UIImage(named: "kiwi") as UIImage?
            let image2  = UIImage(named: "Orange") as UIImage?
            let image3 = UIImage(named: "pineapple") as UIImage?
            let image4  = UIImage(named: "strawberries") as UIImage?
            
            btnQuestion1.setBackgroundImage(image2, for: UIControlState.normal)
            btnQuestion2.setBackgroundImage(image1, for: UIControlState.normal)
            btnQuestion3.setBackgroundImage(image4, for: UIControlState.normal)
            btnQuestion4.setBackgroundImage(image3, for: UIControlState.normal)
            CorrectAnswer = "4"
            
            break
        case 3:
            lblQuestion.text = "Where is the Grapes? "
            //let string = "Where is the Strawberries?!"
            let utterance = AVSpeechUtterance(string:lblQuestion.text!)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)

            let image1  = UIImage(named: "apple") as UIImage?
            let image2  = UIImage(named: "banana") as UIImage?
            let image3 = UIImage(named: "cherries") as UIImage?
            let image4  = UIImage(named: "Grapes") as UIImage?
            
            btnQuestion1.setBackgroundImage(image4, for: UIControlState.normal)
            btnQuestion2.setBackgroundImage(image1, for: UIControlState.normal)
            btnQuestion3.setBackgroundImage(image2, for: UIControlState.normal)
            btnQuestion4.setBackgroundImage(image3, for: UIControlState.normal)
            CorrectAnswer = "1"
            
            break
        case 4:
            lblQuestion.text = "Where is the kiwi?"
           // let string = "Where is the kiwi?!"
            let utterance = AVSpeechUtterance(string:lblQuestion.text!)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)

            let image1  = UIImage(named: "kiwi") as UIImage?
            let image2  = UIImage(named: "Orange") as UIImage?
            let image3 = UIImage(named: "pineapple") as UIImage?
            let image4  = UIImage(named: "strawberries") as UIImage?

            btnQuestion1.setBackgroundImage(image3, for: UIControlState.normal)
            btnQuestion2.setBackgroundImage(image4, for: UIControlState.normal)
            btnQuestion3.setBackgroundImage(image1, for: UIControlState.normal)
            btnQuestion4.setBackgroundImage(image2, for: UIControlState.normal)
            CorrectAnswer = "3"
            
            break
            
        case 5:
            lblQuestion.text = "Where is the Cherries?"
            // let string = "Where is the kiwi?!"
            let utterance = AVSpeechUtterance(string:lblQuestion.text!)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)
            
            let image1  = UIImage(named: "kiwi") as UIImage?
            let image2  = UIImage(named: "cherries") as UIImage?
            let image3 = UIImage(named: "pineapple") as UIImage?
            let image4  = UIImage(named: "strawberries") as UIImage?
            
            btnQuestion1.setBackgroundImage(image3, for: UIControlState.normal)
            btnQuestion2.setBackgroundImage(image2, for: UIControlState.normal)
            btnQuestion3.setBackgroundImage(image1, for: UIControlState.normal)
            btnQuestion4.setBackgroundImage(image4, for: UIControlState.normal)
            CorrectAnswer = "2"
            
            break

        default:
            break
        }
    }
    
    func Hide (){
        lblAnswer.isHidden = true
        //Next.hidden = true
    }
    
    func UnHide () {
        lblAnswer.isHidden = false
        //Next.hidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnQuestion1Action(_ sender: Any) {
        if (CorrectAnswer == "1") {
            lblAnswer.text = "Correct"
            print("pressed")
            
            let string = "Correct!"
            let utterance = AVSpeechUtterance(string:string)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)
            
            RamdomQuestions()
        }
        else{
            
            let image = UIImage(named: "wrong1")
        
            btnQuestion1.setImage(image, for: UIControlState.highlighted)

            lblAnswer.text = "Wrong"
            let string = "Wrong!"
            let utterance = AVSpeechUtterance(string:string)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)
        
        }
    }
    
    @IBAction func btnQuestion2Action(_ sender: Any) {
        if (CorrectAnswer == "2") {
            lblAnswer.text = "Correct"
            let string = "Correct!"
            let utterance = AVSpeechUtterance(string:string)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)
            
            RamdomQuestions()
        }
        else{
            
            let image = UIImage(named: "wrong1")
            btnQuestion2.setImage(image, for: UIControlState.highlighted)
            
            lblAnswer.text = "Wrong"
            let string = "Wrong!"
            let utterance = AVSpeechUtterance(string:string)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)
            

        }
    }
    
    @IBAction func btnQuestion3Action(_ sender: Any) {
        if (CorrectAnswer == "3") {
            lblAnswer.text = "Correct"
            let string = "Correct!"
            let utterance = AVSpeechUtterance(string:string)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)
            
            RamdomQuestions()
        }
        else{
            let image = UIImage(named: "wrong1")
            
            btnQuestion3.setImage(image, for: UIControlState.highlighted)
            
            lblAnswer.text = "Wrong"
            let string = "Wrong!"
            let utterance = AVSpeechUtterance(string:string)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)

        }
    }
    
    @IBAction func btnQuestion4Action(_ sender: Any) {
        if (CorrectAnswer == "4") {
            lblAnswer.text = "Correct"
            let string = "Correct!"
            let utterance = AVSpeechUtterance(string:string)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)
            
            RamdomQuestions()
        }
        else{
            let image = UIImage(named: "wrong1")
            btnQuestion4.setImage(image, for: UIControlState.highlighted)
            
            lblAnswer.text = "Wrong"
            let string = "Wrong!"
            let utterance = AVSpeechUtterance(string:string)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance)

        }
    }
    

    @IBAction func btnQuitPress(_ sender: Any) {
        
        displayMyAlert(message: "")
    }
    
    
    func displayMyAlert(message: String){
        
        let myAlert = UIAlertController(title: "Quiz", message: "Are you sure you wan't to Quit?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default){ action in
            
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
        let noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default){ action in
            
            }
        myAlert.addAction(okAction)
        myAlert.addAction(noAction)
        
        self.present(myAlert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
