//
//  FlowerViewController.swift
//  Kids_Learn_App
//
//  Created by NAGESH RANGAPURE on 16/08/17.
//  Copyright © 2017 Nagesh Rangapure. All rights reserved.
//

import UIKit

class FlowerViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate{
    
    @IBOutlet weak var flowerView: UICollectionView!
    
    
    var imageArray = [UIImage(named: "Rose"), UIImage(named: "hibiscus"),UIImage(named: "marigold"),UIImage(named: "jasmine"),UIImage(named: "sunflower"),UIImage(named: "lavender"),UIImage(named: "Lily"),UIImage(named: "lotus")]
    
    let flowerlist = ["ROSE","HIBISCUS","MARIGOLD","JASMINE","SUNFLOWER","LAVENDER","LILY","LOTUS"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.flowerView.isPagingEnabled = true
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! FlowerCollectionViewCell
        
        cell.imgs.image = imageArray[indexPath.row]
        
        cell.lblName.text = flowerlist[indexPath.row]
        
        cell.btnNext.isHidden = (indexPath.row == collectionView.numberOfItems(inSection: 0) - 1)
        cell.btnPrevious.isHidden = (indexPath.row == 0)
        return  cell
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnClickBack(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    @IBAction func btnClickedNext(_ sender: Any) {
        
        let collectionBounds = self.flowerView.bounds
        let contentOffset = CGFloat(floor(self.flowerView.contentOffset.x + collectionBounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    
    
    @IBAction func btnClickedPrevious(_ sender: Any) {
        let collectionBounds = self.flowerView.bounds
        let contentOffset = CGFloat(floor(self.flowerView.contentOffset.x - collectionBounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    
    func moveToFrame(contentOffset : CGFloat) {
        
        let frame: CGRect = CGRect(x : contentOffset ,y : self.flowerView.contentOffset.y ,width : self.flowerView.frame.width,height : self.flowerView.frame.height)
        self.flowerView.scrollRectToVisible(frame, animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
