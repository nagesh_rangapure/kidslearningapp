//
//  AnimalViewController.swift
//  Kids_Learn_App
//
//  Created by NAGESH RANGAPURE on 16/08/17.
//  Copyright © 2017 Nagesh Rangapure. All rights reserved.
//

import UIKit

class AnimalViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet weak var animalView: UICollectionView!
    
    let animallist = [ UIImage(named:"dog"),UIImage(named:"cat"),UIImage(named:"fish"),UIImage(named:"cow"),UIImage(named:"donkey"),UIImage(named:"goat"),UIImage(named:"monkey"),UIImage(named:"horuse"),UIImage(named:"elephant"),UIImage(named:"lion"),UIImage(named:"tiger")]
    
    let animalName = ["DOG","CAT","FISH","COW","DONKEY","GOAT","MONKEY","HORUSE","ELEPHANT","LION","TIGER"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.animalView.isPagingEnabled = true
        
        self.navigationController?.isNavigationBarHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return animallist.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "animalCell", for: indexPath) as! AnimalCollectionViewCell
        
        cell.imgAnimal.image = animallist[indexPath.row]
        cell.lblName.text = animalName[indexPath.row]
        
        cell.btnNext.isHidden = (indexPath.row == collectionView.numberOfItems(inSection: 0) - 1)
        cell.btnPrevious.isHidden = (indexPath.row == 0)
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnClickBack(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnClickedNext(_ sender: Any) {
        
        let collectionBounds = self.animalView.bounds
        let contentOffset = CGFloat(floor(self.animalView.contentOffset.x + collectionBounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    
    @IBAction func btnClickedPrevious(_ sender: Any) {
        let collectionBounds = self.animalView.bounds
        let contentOffset = CGFloat(floor(self.animalView.contentOffset.x - collectionBounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    
    func moveToFrame(contentOffset : CGFloat) {
        
        let frame: CGRect = CGRect(x : contentOffset ,y : self.animalView.contentOffset.y ,width : self.animalView.frame.width,height : self.animalView.frame.height)
        self.animalView.scrollRectToVisible(frame, animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
