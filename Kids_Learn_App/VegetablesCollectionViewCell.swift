//
//  VegetablesCollectionViewCell.swift
//  Kids_Learn_App
//
//  Created by NAGESH RANGAPURE on 21/08/17.
//  Copyright © 2017 Nagesh Rangapure. All rights reserved.
//

import UIKit

class VegetablesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgVegetable: UIImageView!
    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var btnPrevious: UIButton!
}

