//
//  VegetableQuizeViewController.swift
//  Kids_Learn_App
//
//  Created by NAGESH RANGAPURE on 08/09/17.
//  Copyright © 2017 Nagesh Rangapure. All rights reserved.
//

import UIKit

class VegetableQuizeViewController: UIViewController {

    @IBOutlet weak var lblQustion: UILabel!
    
    @IBOutlet weak var btnQustion1: UIButton!
    
    @IBOutlet weak var btnQustion2: UIButton!
    
    @IBOutlet weak var btnQustion3: UIButton!
    
    @IBOutlet weak var btnQustion4: UIButton!
    
    @IBOutlet weak var lblAnswer: UILabel!
    var CorrectAnswer = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        Hide()
        RamdomQuestions()
        
        // Do any additional setup after loading the view.
    }
    
    func RamdomQuestions () {
        var RandomNumber = arc4random() % 4
        RandomNumber += 1
        
        switch (RandomNumber) {
            
        case 1:
            lblQustion.text = "Where is the Cabbage? "
            
            let image1  = UIImage(named: "cabbage") as UIImage?
            let image2  = UIImage(named: "carrot") as UIImage?
            let image3  = UIImage(named: "cucumber") as UIImage?
            let image4  = UIImage(named: "Brinjal") as UIImage?
            
            btnQustion1.setBackgroundImage(image2, for: UIControlState.normal)
            btnQustion2.setBackgroundImage(image1, for: UIControlState.normal)
            btnQustion3.setBackgroundImage(image4, for: UIControlState.normal)
            btnQustion4.setBackgroundImage(image3, for: UIControlState.normal)
            CorrectAnswer = "2"
            
            break
        case 2:
            lblQustion.text = "Where is the Carrot? "
            
            let image1  = UIImage(named: "cabbage") as UIImage?
            let image2  = UIImage(named: "carrot") as UIImage?
            let image3  = UIImage(named: "cucumber") as UIImage?
            let image4  = UIImage(named: "Brinjal") as UIImage?

            
            btnQustion1.setBackgroundImage(image3, for: UIControlState.normal)
            btnQustion2.setBackgroundImage(image1, for: UIControlState.normal)
            btnQustion3.setBackgroundImage(image4, for: UIControlState.normal)
            btnQustion4.setBackgroundImage(image2, for: UIControlState.normal)
            CorrectAnswer = "4"
            
            break
        case 3:
            lblQustion.text = "Where is the Brinjal? "
            
            let image1  = UIImage(named: "cabbage") as UIImage?
            let image2  = UIImage(named: "carrot") as UIImage?
            let image3  = UIImage(named: "cucumber") as UIImage?
            let image4  = UIImage(named: "Brinjal") as UIImage?

            
            btnQustion1.setBackgroundImage(image4, for: UIControlState.normal)
            btnQustion2.setBackgroundImage(image1, for: UIControlState.normal)
            btnQustion3.setBackgroundImage(image3, for: UIControlState.normal)
            btnQustion4.setBackgroundImage(image2, for: UIControlState.normal)
            CorrectAnswer = "1"
            break
        case 4:
            lblQustion.text = "Where is the cucumbers?"
            
            let image1  = UIImage(named: "cabbage") as UIImage?
            let image2  = UIImage(named: "carrot") as UIImage?
            let image3  = UIImage(named: "cucumber") as UIImage?
            let image4  = UIImage(named: "Brinjal") as UIImage?
            
            btnQustion1.setBackgroundImage(image4, for: UIControlState.normal)
            btnQustion2.setBackgroundImage(image1, for: UIControlState.normal)
            btnQustion3.setBackgroundImage(image3, for: UIControlState.normal)
            btnQustion4.setBackgroundImage(image2, for: UIControlState.normal)
            CorrectAnswer = "3"
            break
            
        default:
            break
        }
    }
    
    func Hide (){
        lblAnswer.isHidden = true
        //Next.hidden = true
    }
    
    func UnHide () {
        lblAnswer.isHidden = false
        //Next.hidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnQustion1Action(_ sender: Any) {
        
        if (CorrectAnswer == "1") {
            lblAnswer.text = "Correct"
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sbPopUpID") as! PopUpViewController
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            
            RamdomQuestions()
        }
        else{
            lblAnswer.text = "Wrong"
        }
    }

    @IBAction func btnQustion2Action(_ sender: Any) {
        
        if (CorrectAnswer == "2") {
            lblAnswer.text = "Correct"
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sbPopUpID") as! PopUpViewController
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)

            RamdomQuestions()
        }
        else{
            lblAnswer.text = "Wrong"
        }
    }
    @IBAction func btnQustion3Action(_ sender: Any) {
        
        if (CorrectAnswer == "3") {
            lblAnswer.text = "Correct"
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sbPopUpID") as! PopUpViewController
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            

            RamdomQuestions()
        }
        else{
            lblAnswer.text = "Wrong"
            
        }
    }
    @IBAction func btnQustion4Action(_ sender: Any) {
        
        if (CorrectAnswer == "4") {
            lblAnswer.text = "Correct"
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sbPopUpID") as! PopUpViewController
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)

            RamdomQuestions()
        }
        else{
            lblAnswer.text = "Wrong"
        }    }
    
    @IBAction func btnQuitPress(_ sender: Any) {
        
        displayMyAlert(message: "")
    }
    
    
    func displayMyAlert(message:String){
        
        let myAlert = UIAlertController(title: "Quiz", message: "Are you sure you wan't to Quit?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default){ action in
        
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default){ action in
        }
        myAlert.addAction(okAction)
        myAlert.addAction(noAction)
        self.present(myAlert, animated: true, completion: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
