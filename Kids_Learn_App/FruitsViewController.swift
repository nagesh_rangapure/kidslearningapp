//
//  FruitsViewController.swift
//  Kids_Learn_App
//
//  Created by NAGESH RANGAPURE on 16/08/17.
//  Copyright © 2017 Nagesh Rangapure. All rights reserved.
//

import UIKit
import AVFoundation

class FruitsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource{
    
    
    @IBOutlet weak var fruitCollectionView: UICollectionView!
    var counter = 0
    var imagelist = [UIImage(named: "apple"),UIImage(named:"banana"),UIImage(named:"Orange"),UIImage(named:"Grapes"),UIImage(named: "pineapple"),UIImage(named: "cherries"),UIImage(named: "kiwi"),UIImage(named:"strawberries")]
    
    
   let imageName = ["APPLE","BANANA","ORANGE","GRAPES","PINEAPPLE","CHERRY","KIWI","STRAWBERRY"]

    let btnName = ["APPLE","BANANA","ORANGE","GRAPES","PINEAPPLE","CHERRY","KIWI","STRAWBERRY"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let string = "apple!"
//        let utterance = AVSpeechUtterance(string:string)
//        utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
//        
//        let synth = AVSpeechSynthesizer()
//        synth.speak(utterance)
//
        
        self.fruitCollectionView.isPagingEnabled = true

        self.navigationController?.isNavigationBarHidden = true

        
        // Do any additional setup after loading the view.
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagelist.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FruitCell", for: indexPath) as! FruitsCollectionViewCell
        
        cell.imgFruit.image = imagelist[indexPath.row]
        cell.lblName.text = imageName[indexPath.row]
        
        cell.btnNext.isHidden = (indexPath.row == collectionView.numberOfItems(inSection: 0) - 1)
        cell.btnPrevious.isHidden = (indexPath.row == 0)
       
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnClickedToPlay(_ sender: Any) {
        print("pressed")
        
        let string = "apple!"
        let utterance = AVSpeechUtterance(string:string)
        utterance.voice = AVSpeechSynthesisVoice(language: "en-US")

        let synth = AVSpeechSynthesizer()
        synth.speak(utterance)
        
    }
    
    @IBAction func btnClickback(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnClickedNext(_ sender: UIButton) {
  
        let collectionBounds = self.fruitCollectionView.bounds
        let contentOffset = CGFloat(floor(self.fruitCollectionView.contentOffset.x + collectionBounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
        
    }
    
    
    @IBAction func btnClickedPrev(_ sender: Any) {
        
        let collectionBounds = self.fruitCollectionView.bounds
        let contentOffset = CGFloat(floor(self.fruitCollectionView.contentOffset.x - collectionBounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
        
    }

    func moveToFrame(contentOffset : CGFloat) {
        
        let frame: CGRect = CGRect(x : contentOffset ,y : self.fruitCollectionView.contentOffset.y ,width : self.fruitCollectionView.frame.width,height : self.fruitCollectionView.frame.height)
        self.fruitCollectionView.scrollRectToVisible(frame, animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
