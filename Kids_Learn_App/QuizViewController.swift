//
//  QuizViewController.swift
//  Kids_Learn_App
//
//  Created by NAGESH RANGAPURE on 31/08/17.
//  Copyright © 2017 Nagesh Rangapure. All rights reserved.
//

import UIKit

class QuizViewController: UIViewController {

    @IBOutlet weak var gif: UIImageView!
    
    @IBOutlet weak var web: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        gif.loadGif(name: "animated-duck-image-0022")
        
        
        let url = Bundle.main.url(forResource: "animated-duck-image-0022", withExtension: "gif")!
        let data = try! Data(contentsOf: url)
        web.load(data, mimeType: "image/gif", textEncodingName: "UTF-8", baseURL: NSURL() as URL)
        web.scalesPageToFit = true
        web.contentMode = UIViewContentMode.scaleAspectFit
        //gif.animationDuration
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnQuitAction(_ sender: Any) {
    }
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
